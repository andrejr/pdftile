#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import math
import re
import subprocess
import sys
from dataclasses import dataclass
from decimal import Decimal
from shutil import copyfile
from tempfile import NamedTemporaryFile
from textwrap import dedent
from typing import Tuple
from typing import Type
from typing import TypeVar
from typing import Union

from PyPDF2 import PdfFileReader
from PyPDF2 import PdfFileWriter
from PyPDF2.generic import RectangleObject
from PyPDF2.pdf import PageObject

T = TypeVar("T", bound="Box")


def mm_to_pspt(length: Union[Decimal, int]) -> Decimal:
    return length * Decimal(2.83465)


one_cm_in_pspt = mm_to_pspt(10)
seven_mm_in_pspt = mm_to_pspt(7)


def insert_page_into_page(
    source: PageObject,
    src_offs: Tuple[Decimal, Decimal],
    destination: PageObject,
    dst_offs: Tuple[Decimal, Decimal],
):
    so_x, so_y = src_offs
    do_x, do_y = dst_offs

    destination.mergeTranslatedPage(source, do_x - so_x, do_y - so_y)


@dataclass
class Box:
    left: Decimal
    bottom: Decimal
    right: Decimal
    top: Decimal

    @classmethod
    def from_ro(cls: Type[T], ro: RectangleObject) -> T:
        left, bottom = ro.lowerLeft
        right, top = ro.upperRight
        return cls(left, bottom, right, top)

    def to_ro(self) -> RectangleObject:
        return RectangleObject([self.left, self.bottom, self.right, self.top])

    @property
    def size(self) -> Tuple[Decimal, Decimal]:
        return (abs(self.left - self.right), abs(self.top - self.bottom))


def ghostscript_calculate_page_bbox(page: PageObject) -> RectangleObject:

    temp_out = PdfFileWriter()
    temp_out.addPage(page)

    with NamedTemporaryFile("wb", buffering=0, suffix=".pdf") as tmp_pdf:
        temp_out.write(tmp_pdf)

        gs_proc_return = subprocess.run(
            [
                "gs",
                "-dSAFER",
                "-dNOPAUSE",
                "-dBATCH",
                "-sDEVICE=bbox",
                tmp_pdf.name,
            ],
            capture_output=True,
        )
        stderr_out = gs_proc_return.stderr.decode("utf-8")
        search_group = re.search(
            r"^%%HiResBoundingBox: (.+) (.+) (.+) (.+)",
            stderr_out,
            re.MULTILINE | re.IGNORECASE,
        )

        if not search_group:
            raise RuntimeError("Unable to parse Ghostscript output.")

        return Box(*(Decimal(val) for val in search_group.groups())).to_ro()


def tile_page_contents(
    page: PageObject,
    out_dimensions: Tuple[Decimal, Decimal] = None,
    margin: Decimal = seven_mm_in_pspt,
) -> PdfFileWriter:
    if not out_dimensions:
        dimensions = Box.from_ro(page.cropBox)
        out_dimensions = dimensions.size

    width, height = out_dimensions

    new_page = PageObject.createBlankPage(width=width, height=height)

    source_rect: RectangleObject = ghostscript_calculate_page_bbox(page)
    dest_rect = new_page.cropBox

    tile_ct_x, tile_ct_y = tile_count(source_rect, dest_rect, margin)
    if tile_ct_x == 0 or tile_ct_y == 0:
        raise RuntimeError(
            "Unable to fit even a single input object into output."
        )
    opt_margin_x, opt_margin_y = calculate_optimum_margin(
        source_rect, dest_rect, margin
    )

    input_offset = source_rect.lowerLeft

    for y_i in range(tile_ct_y):
        y_coord = opt_margin_y + y_i * (
            opt_margin_y * 2 + source_rect.getHeight()
        )
        for x_i in range(tile_ct_x):
            x_coord = opt_margin_x + x_i * (
                opt_margin_x * 2 + source_rect.getWidth()
            )
            insert_page_into_page(
                page, input_offset, new_page, (x_coord, y_coord)
            )

    return new_page


def tile_pdf(
    reader: PdfFileReader,
    out_dimensions: Tuple[Decimal, Decimal] = None,
    margin: Decimal = seven_mm_in_pspt,
) -> PdfFileWriter:
    writer = PdfFileWriter()
    for page in reader.pages:
        new_page = tile_page_contents(page, out_dimensions, margin)
        writer.addPage(new_page)

    return writer


def tile_count(
    src_page_rect: RectangleObject,
    dst_page_rect: RectangleObject,
    margin: Decimal = one_cm_in_pspt / 2,
) -> Tuple[int, int]:
    src_page_box = Box.from_ro(src_page_rect)
    dst_page_box = Box.from_ro(dst_page_rect)

    src_page_width, src_page_height = src_page_box.size
    dst_page_width, dst_page_height = dst_page_box.size

    def quotient(dst_dim: Decimal, src_dim: Decimal) -> int:
        padding = 2 * margin
        # padding added back because last item needs no padding:
        # n items => n - 1 paddings
        available_width = dst_dim - 2 * margin + padding
        item_min_width = src_dim + padding
        return math.floor(available_width / item_min_width)

    w_inst = quotient(dst_page_width, src_page_width)
    h_inst = quotient(dst_page_height, src_page_height)
    return (w_inst, h_inst)


def calculate_optimum_margin(
    src_page_rect: RectangleObject,
    dst_page_rect: RectangleObject,
    minimum_margin: Decimal = one_cm_in_pspt / 2,
) -> Tuple[Decimal, Decimal]:
    src_page_box = Box.from_ro(src_page_rect)
    dst_page_box = Box.from_ro(dst_page_rect)

    src_page_w, src_page_h = src_page_box.size
    dst_page_w, dst_page_h = dst_page_box.size

    w_inst, h_inst = tile_count(src_page_rect, dst_page_rect, minimum_margin)

    w_total_margin = dst_page_w - src_page_w * w_inst
    h_total_margin = dst_page_h - src_page_h * h_inst

    w_single_margin = (w_total_margin / w_inst) / 2
    h_single_margin = (h_total_margin / h_inst) / 2

    return w_single_margin, h_single_margin


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description=dedent(
            """\
        PDF tiling utility.

        This utility goes through a PDF and does the following for every page:
        it extracts the non-white contents of PDF pages and tiles them onto the
        page, respecting the `margin` provided.
        It outputs pages filled with tiled contents of the original page.


        The original usecase was to print out multiple tests (for school exams)
        onto a single page.
            """
        )
    )

    parser.add_argument(
        "infile", type=argparse.FileType("rb"), help="Input file path.",
    )

    outgroup = parser.add_mutually_exclusive_group(required=True)

    outgroup.add_argument(
        "-o",
        "--outfile",
        type=argparse.FileType("wb"),
        default=sys.stdout,
        help="Output file path.",
    )
    outgroup.add_argument(
        "-i",
        "--in-place",
        action="store_true",
        help="Modify the input file in place.",
    )
    parser.add_argument(
        "-m",
        "--margin",
        type=Decimal,
        default=Decimal(7),
        help="Margin in output file, provided in mm (7mm by default).",
    )
    parser.add_argument(
        "-d",
        "--output-dimensions",
        type=Decimal,
        nargs=2,
        default=None,
        help="Output PDF dimensions, WIDTH and HEIGHT, in mm. By default set to "
        "be the same as input file dimensions.",
    )
    # parser.add_argument('-m', '--margin', type=Decimal)

    return parser


def main():

    parser = create_parser()
    args = parser.parse_args()

    if args.output_dimensions:
        args.output_dimensions = [
            mm_to_pspt(dim) for dim in args.output_dimensions
        ]

    with args.infile as infile, args.outfile as outfile:

        # if the wanted end result is to overwrite the input file, write to a
        # temporary file instead
        if infile == outfile or args.in_place:
            outfile = NamedTemporaryFile("wb", buffering=0, suffix=".pdf")

        ipdf = PdfFileReader(args.infile)
        ipages = ipdf.getNumPages()
        if ipages != 1:
            raise RuntimeError(
                f"Input PDF file must have exactly one page, not {ipages}."
            )

        writer = tile_pdf(
            ipdf, margin=args.margin, out_dimensions=args.output_dimensions
        )
        writer.write(outfile)

        # replace the initial file with the temporary file in the end
        if infile == outfile or args.in_place:
            copyfile(outfile.name, infile.name)


if __name__ == "__main__":
    main()
